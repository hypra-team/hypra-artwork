hypra-artwork: a collection of resources for the Hypra desktop.

This source package is meant to gather any and all resources
(mainly images) making the Hypra MATE desktop user experience
more pleasant and better-looking.
Please refer to each image’s copyright information to know its
author and its original source.
 
